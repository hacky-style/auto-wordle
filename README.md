# Auto Wordle
### [Chrome Extension](https://chrome.google.com/webstore/detail/auto-wordle/hlhgjnchfkbkmmfbmldahdnoadkmebfk) for solving [Wordle](https://www.nytimes.com/games/wordle/index.html)

![demo](demo.gif)

## Getting started

1. Using git clone or download the zip file
2. Navigate to chrome://extensions
3. Expand the Developer dropdown menu and click “Load Unpacked Extension”
4. Navigate to the local folder containing the extension’s code and click Ok
5. Assuming there are no errors, the extension should load into your browser

## License
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

